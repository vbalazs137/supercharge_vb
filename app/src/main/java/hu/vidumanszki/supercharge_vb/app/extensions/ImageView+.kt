package hu.vidumanszki.supercharge_vb.app.extensions

import android.net.Uri
import android.widget.ImageView
import com.bumptech.glide.Glide


fun ImageView.loadUrl(url: String?) {
    Glide
        .with(context)
        .load(Uri.parse(url ?: ""))
        .into(this)
}