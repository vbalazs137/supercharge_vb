package hu.vidumanszki.supercharge_vb.app.extensions

import android.animation.ObjectAnimator
import android.view.MotionEvent
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator

fun View.show(isShow: Boolean) {
    visibility = if (isShow) View.VISIBLE else View.INVISIBLE
}

fun View.enableHapticFeedback() {
    setOnTouchListener(hapticFeedbackTouchListener)
}

private val hapticFeedbackTouchListener: View.OnTouchListener
    get() = View.OnTouchListener { v, event ->
        if (v.isEnabled && v.hasOnClickListeners()) {
            var delay: Long = 0
            val alphaValue = when (event?.action) {
                MotionEvent.ACTION_DOWN -> {
                    delay = 0
                    0.5f
                }
                MotionEvent.ACTION_UP -> {
                    delay = (ANIMATION_DURATION_CLICK.toFloat() / 2).toLong()
                    1f
                }
                MotionEvent.ACTION_CANCEL -> {
                    delay = (ANIMATION_DURATION_CLICK.toFloat() / 2).toLong()
                    1f
                }
                else -> null
            }

            alphaValue?.let {
                val animation = ObjectAnimator.ofFloat(
                    v,
                    "alpha",
                    alphaValue
                ).apply {
                    duration = ANIMATION_DURATION_CLICK
                    interpolator = AccelerateDecelerateInterpolator()
                    startDelay = delay
                }
                animation.start()
            }

        }
        false
    }

private const val ANIMATION_DURATION_CLICK = 200L