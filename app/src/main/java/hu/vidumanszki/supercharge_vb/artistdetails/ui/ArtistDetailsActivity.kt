package hu.vidumanszki.supercharge_vb.artistdetails.ui

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import dagger.android.AndroidInjection
import hu.vidumanszki.supercharge_vb.R
import hu.vidumanszki.supercharge_vb.app.extensions.loadUrl
import hu.vidumanszki.supercharge_vb.app.extensions.show
import hu.vidumanszki.supercharge_vb.artistdetails.viewmodel.ArtistDetailsViewModel
import hu.vidumanszki.supercharge_vb.di.viewmodel.ViewModelFactory
import hu.vidumanszki.supercharge_vb.main.adapter.ArtistListAdapter
import hu.vidumanszki.supercharge_vb.main.model.Artist
import kotlinx.android.synthetic.main.activity_artist_details.*
import javax.inject.Inject

fun Context.artistDetailsIntent(artist: Artist): Intent {
    return Intent(this, ArtistDetailsActivity::class.java).apply {
        putExtra(KEY_ARTIST, artist)
    }
}

private const val KEY_ARTIST = "key_artist"

class ArtistDetailsActivity : AppCompatActivity() {

    private lateinit var viewModel: ArtistDetailsViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val artist: Artist
        get() = intent.getSerializableExtra(KEY_ARTIST) as Artist

    private val listAdapter = ArtistListAdapter {
        startActivity(artistDetailsIntent(it))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_artist_details)

        viewModel =
            ViewModelProviders.of(this, viewModelFactory).get(ArtistDetailsViewModel::class.java)

        setupUi()
        bindUi()

        viewModel.loadArtists(artist)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onContextItemSelected(item)
        }
    }

    private fun setupUi() {
        setSupportActionBar(tool_bar)
        collapsingToolbar.title = artist.name
        collapsingToolbar.setCollapsedTitleTextColor(Color.WHITE)
        collapsingToolbar.setExpandedTitleColor(Color.WHITE)
        imgArtist.loadUrl(artist.imageUrl)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        with(recyclerView) {
            layoutManager = GridLayoutManager(this@ArtistDetailsActivity, 2)
            adapter = listAdapter
        }
    }

    private fun bindUi() {
        with(viewModel) {
            observe(similarArtists) {
                listAdapter.setArtists(it)
            }
            observe(loading) {
                progressBar.show(it)
            }
            observe(message) {
                tvEmpty.text = it
            }
        }
    }

    private fun <T> observe(liveData: LiveData<T>, f: (T) -> Unit) {
        liveData.observe(this, Observer {
            f.invoke(it)
        })
    }
}
