package hu.vidumanszki.supercharge_vb.artistdetails.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import hu.vidumanszki.supercharge_vb.R
import hu.vidumanszki.supercharge_vb.main.model.Artist
import hu.vidumanszki.supercharge_vb.network.ArtistDeserializer
import hu.vidumanszki.supercharge_vb.network.SCApi
import kotlinx.coroutines.launch
import javax.inject.Inject


class ArtistDetailsViewModel @Inject constructor(
    private val app: Application,
    private val scApi: SCApi,
    private val artistDeserializer: ArtistDeserializer
): ViewModel() {

    val similarArtists = MutableLiveData<List<Artist>>()

    val loading = MutableLiveData<Boolean>()

    val message = MutableLiveData<String>()

    fun loadArtists(artist: Artist) {
        viewModelScope.launch {
            loading.value = true

            val list = try {
                artistDeserializer.getSimilarArtists(
                    scApi.getSimilarArtists(
                        app.getString(R.string.lastfm_api_key),
                        artist.name
                    )
                )
            } catch (e: Exception) {
                null
            }

            similarArtists.value = list

            message.value = when {
                list == null -> app.getString(R.string.artist_search_error)
                list.isEmpty() -> app.getString(R.string.artist_details_no_result)
                else -> null
            }

            loading.value = false
        }
    }

}