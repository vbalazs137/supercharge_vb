package hu.vidumanszki.supercharge_vb.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import hu.vidumanszki.supercharge_vb.artistdetails.ui.ArtistDetailsActivity
import hu.vidumanszki.supercharge_vb.main.ui.MainActivity
import hu.vidumanszki.supercharge_vb.di.scope.PerActivity


@Module
abstract class ActivityModule {

    @PerActivity
    @ContributesAndroidInjector
    internal abstract fun contributeMainActivity(): MainActivity

    @PerActivity
    @ContributesAndroidInjector
    internal abstract fun contributeArtistDetailsActivity(): ArtistDetailsActivity

}