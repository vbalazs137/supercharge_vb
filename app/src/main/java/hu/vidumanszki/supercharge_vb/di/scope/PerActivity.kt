package hu.vidumanszki.supercharge_vb.di.scope

import javax.inject.Scope


@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerActivity