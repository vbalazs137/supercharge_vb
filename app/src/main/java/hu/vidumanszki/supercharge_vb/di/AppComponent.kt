package hu.vidumanszki.supercharge_vb.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import hu.vidumanszki.supercharge_vb.app.SuperchargeVb
import hu.vidumanszki.supercharge_vb.di.module.ActivityModule
import hu.vidumanszki.supercharge_vb.di.module.NetworkModule
import hu.vidumanszki.supercharge_vb.di.module.ViewModelModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ActivityModule::class,
        ViewModelModule::class,
        NetworkModule::class
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: SuperchargeVb)

}