package hu.vidumanszki.supercharge_vb.di.module

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import hu.vidumanszki.supercharge_vb.artistdetails.viewmodel.ArtistDetailsViewModel
import hu.vidumanszki.supercharge_vb.di.viewmodel.ViewModelKey
import hu.vidumanszki.supercharge_vb.main.viewmodel.MainViewModel


@Module
internal abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ArtistDetailsViewModel::class)
    abstract fun bindArtistDetailsViewModel(viewModel: ArtistDetailsViewModel): ViewModel

}