package hu.vidumanszki.supercharge_vb.network

import com.google.gson.internal.LinkedTreeMap
import hu.vidumanszki.supercharge_vb.main.model.Artist


class ArtistDeserializer {

    fun getArtists(result: Any): List<Artist>? {
        val resultObject = (result as LinkedTreeMap<String, Any>)["results"]

        return resultObject?.let {
            getArtistList((it as LinkedTreeMap<String, Any>)["artistmatches"])
        } ?: run {
            null
        }
    }

    fun getSimilarArtists(result: Any): List<Artist>? {
        val resultObject = (result as LinkedTreeMap<String, Any>)["artist"]

        return resultObject?.let {
            getArtistList((it as LinkedTreeMap<String, Any>)["similar"])
        } ?: run {
            null
        }
    }

    private fun getArtistList(artistJson: Any?): List<Artist>? {
        artistJson ?: return null

        return try {
            val artists = (artistJson as LinkedTreeMap<String, ArrayList<Any>>)["artist"]

            val artistList = ArrayList<Artist>()

            artists?.forEach {artistObject ->
                (artistObject as LinkedTreeMap<String, Any>)

                val name = artistObject["name"] as String
                val listeners = artistObject["listeners"] as? String
                val image =
                    (artistObject["image"] as ArrayList<LinkedTreeMap<String, String>>)[1]["#text"]

                artistList.add(Artist(name, listeners ?: "", image))
            }
            artistList
        } catch (e: Exception) {
            return null
        }
    }

}