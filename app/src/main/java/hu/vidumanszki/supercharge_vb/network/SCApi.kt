package hu.vidumanszki.supercharge_vb.network

import retrofit2.http.GET
import retrofit2.http.Query


interface SCApi {

    @GET("?method=artist.search")
    suspend fun searchArtist(
        @Query("api_key") apiKey: String,
        @Query ("artist") artist: String,
        @Query("format") format: String = "json"
    ): Any

    @GET("?method=artist.getinfo")
    suspend fun getSimilarArtists(
        @Query("api_key") apiKey: String,
        @Query ("artist") artist: String,
        @Query("format") format: String = "json"
    ): Any

    companion object {
        const val BASE_URL = "http://ws.audioscrobbler.com/2.0/"
    }

}