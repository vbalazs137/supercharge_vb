package hu.vidumanszki.supercharge_vb.main.model

import java.io.Serializable


data class Artist(
    val name: String,
    val listeners: String,
    val imageUrl: String?
): Serializable