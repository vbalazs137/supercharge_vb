package hu.vidumanszki.supercharge_vb.main.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import hu.vidumanszki.supercharge_vb.R
import hu.vidumanszki.supercharge_vb.main.model.Artist
import hu.vidumanszki.supercharge_vb.network.ArtistDeserializer
import hu.vidumanszki.supercharge_vb.network.SCApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject


class MainViewModel @Inject constructor(
    private val app: Application,
    private val scApi: SCApi,
    private val artistDeserializer: ArtistDeserializer
) : ViewModel() {

    private var searchArtistJob: Job? = null

    private var queryText: String? = null

    val artists = MutableLiveData<List<Artist>>()

    val loading = MutableLiveData<Boolean>()

    val message = MutableLiveData<String>()

    init {
        setupEmptyMessage()
    }

    fun search(artist: String?) {
        queryText = artist
        searchArtistJob?.cancel()

        searchArtistJob = viewModelScope.launch {
            loading.value = true

            delay(400)

            try {
                 val list = artistDeserializer.getArtists(
                    scApi.searchArtist(
                        app.getString(R.string.lastfm_api_key),
                        artist ?: ""
                    )
                )

                artists.value = list
                setupEmptyMessage()
            } catch (e: Exception) {
                artists.value = null
                message.value = app.getString(R.string.artist_search_error)
            }

            loading.value = false
        }
    }

    private fun setupEmptyMessage() {
        message.value =
            if (artists.value.isNullOrEmpty()) {
                if (queryText.isNullOrEmpty()) {
                    app.getString(R.string.artist_search_message)
                } else  {
                    app.getString(R.string.artist_search_no_result)
                }
            } else {
                null
            }
    }

}