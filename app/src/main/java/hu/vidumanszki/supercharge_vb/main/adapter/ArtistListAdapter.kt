package hu.vidumanszki.supercharge_vb.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hu.vidumanszki.supercharge_vb.R
import hu.vidumanszki.supercharge_vb.app.extensions.enableHapticFeedback
import hu.vidumanszki.supercharge_vb.app.extensions.loadUrl
import hu.vidumanszki.supercharge_vb.app.extensions.show
import hu.vidumanszki.supercharge_vb.main.model.Artist
import kotlinx.android.synthetic.main.row_artist.view.*


class ArtistListAdapter(
    private val itemClickListener: (Artist) -> Unit
) : RecyclerView.Adapter<ArtistListAdapter.ViewHolder>() {

    private val items = ArrayList<Artist>()

    fun setArtists(artists: List<Artist>?) {
        items.clear()

        artists?.let {
            items.addAll(it)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.row_artist, parent, false)
        )
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: Artist) {
            with(itemView) {
                setOnClickListener {
                    itemClickListener.invoke(item)
                }
                itemView.enableHapticFeedback()

                tvName.text = item.name
                imgArtist.loadUrl(item.imageUrl)

                val listenersText =
                    "${item.listeners} ${context.getString(R.string.artist_listeners)}"
                tvListeners.text = listenersText

                tvListeners.show(item.listeners.isNotEmpty())
            }
        }
    }

}