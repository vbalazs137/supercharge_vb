package hu.vidumanszki.supercharge_vb.main.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import dagger.android.AndroidInjection
import hu.vidumanszki.supercharge_vb.R
import hu.vidumanszki.supercharge_vb.app.extensions.show
import hu.vidumanszki.supercharge_vb.artistdetails.ui.artistDetailsIntent
import hu.vidumanszki.supercharge_vb.di.viewmodel.ViewModelFactory
import hu.vidumanszki.supercharge_vb.main.adapter.ArtistListAdapter
import hu.vidumanszki.supercharge_vb.main.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    private lateinit var mainViewModel: MainViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val listAdapter = ArtistListAdapter {
        startActivity(artistDetailsIntent(it))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainViewModel =
            ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)

        setupUi()
        bindUi()
    }

    private fun setupUi() {
        with(recyclerView) {
            layoutManager = GridLayoutManager(this@MainActivity, 2)
            adapter = listAdapter
        }

        searchView.setIconifiedByDefault(false)
    }

    private fun bindUi() {
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                search(query)

                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                search(newText)

                return true
            }
        })

        with(mainViewModel) {
            observe(artists) {
                listAdapter.setArtists(it)
            }
            observe(loading) {
                progressBar.show(it)
            }
            observe(message) {
                tvEmpty.text = it
            }
        }
    }

    private fun search(query: String?) {
        mainViewModel.search(query)
    }

    private fun <T> observe(liveData: LiveData<T>, f: (T) -> Unit) {
        liveData.observe(this, Observer {
            f.invoke(it)
        })
    }
}
